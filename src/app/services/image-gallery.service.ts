import { Injectable } from '@angular/core';
import { ImageGallery } from '../ImageGallery';
@Injectable()
export class ImageGalleryService {
  private images: ImageGallery[] = [
    {
      description:'Desk Image',
      url:'https://m.media-amazon.com/images/I/71-zstyfZ+L._AC_SX466_.jpg',
      clickedDescription:false
    },
    {
      description:'Laptop Image',
      url: 'https://m.media-amazon.com/images/I/81SIr70ep2L._AC_SX450_.jpg',
      clickedDescription:false
    },
    {
      description:'Window Image',
      url:'https://images.unsplash.com/photo-1527352774566-e4916e36c645?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8d2luZG93JTIwZnJhbWV8ZW58MHx8MHx8&w=1000&q=80',
      clickedDescription:false
    },
    {
      description:'Coffee Image',
      url:'https://images.pexels.com/photos/312418/pexels-photo-312418.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      clickedDescription:false
    }
    ]
  constructor() { }
  getImageItems(): ImageGallery[]{
    return  this.images;
  }
  toggleDescriptionVisibility(image:ImageGallery):void{
    image.clickedDescription = !image.clickedDescription;
  }
  displayedTexts: number = 0;
  increaseCounter(image:ImageGallery):number{
    if(image.clickedDescription){
      this.displayedTexts++;
    }else this.displayedTexts--;
    return this.displayedTexts;
  }
}
