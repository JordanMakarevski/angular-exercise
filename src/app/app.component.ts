import { Component, OnInit } from '@angular/core';
import { ImageGallery } from './ImageGallery';
import { ImageGalleryService } from './services/image-gallery.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers:[ImageGalleryService]
})

export class AppComponent implements OnInit{
  displayedTexts:number = 0;

  images: ImageGallery[] = [];
  
  constructor(private imageGalleryService: ImageGalleryService){   }
  ngOnInit(): void {
    this.images = this.imageGalleryService.getImageItems();
  }
  updateCounter(image:ImageGallery) : void{
   this.imageGalleryService.toggleDescriptionVisibility(image);
   this.displayedTexts=this.imageGalleryService.increaseCounter(image);
}
}

