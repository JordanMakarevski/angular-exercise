export interface ImageGallery{
    description:string;
    url:string;
    clickedDescription : boolean;
}