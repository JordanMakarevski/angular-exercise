import { Component, Input, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { ImageGallery } from '../ImageGallery';
import { ImageGalleryService } from '../services/image-gallery.service';
@Component({
  selector: 'app-image-gallery',
  templateUrl: './image-gallery.component.html',
  styleUrls: ['./image-gallery.component.scss']
})
export class ImageGalleryComponent implements OnInit {

@Input() 
image: ImageGallery | undefined = undefined;

@Output() 
doneEvent: EventEmitter<ImageGallery> = new EventEmitter<ImageGallery>();

constructor() { }

  ngOnInit(): void {
  }
clickToggle(){
  this.doneEvent.emit(this.image)
}

}
